#include <SimpleDHT.h>
#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <RestClient.h>

// Consts
int pinDHT22 = 0;
SimpleDHT22 dht22(pinDHT22);
RestClient client = RestClient("149.28.108.187");

void setup() {
  // Debug
  Serial.begin(9600);

  // Wifi
  WiFiManager wifiManager;
  wifiManager.autoConnect("ARDUINO");
}

String response;
void loop() {
  // Temperature and humidity measurement
  float temperature = 0;
  float humidity = 0;
  int err = SimpleDHTErrSuccess;
  if ((err = dht22.read2(&temperature, &humidity, NULL)) != SimpleDHTErrSuccess) {
    Serial.print("Read DHT22 failed, err="); Serial.println(err);delay(2000);
    return;
  }
  Serial.print((float)temperature); Serial.print(" *C, ");
  Serial.print((float)humidity); Serial.println(" RH%");
  
  // Measuring earth moisture
  Serial.println(analogRead(A0));
  int terra = analogRead(A0);

  // Make data payload
  response = "";
  String postData = String("{\"earth_moisture\": ") + String(terra) + String(",\"temperature\": ") + temperature + String(",\"moisture\": ") + String(humidity) + String("}");
  char cPostData[100];
  postData.toCharArray(cPostData, 100);

  // Make request
  client.setHeader("Content-Type: application/javascript");
  int statusCode = client.post("/arduino.json", cPostData, &response);
  Serial.print("Status code from server: ");
  Serial.println(statusCode);
  Serial.print("Response body from server: ");
  Serial.println(response);

  // Loop time
  Serial.println("=================================");
  delay(5000);
}
